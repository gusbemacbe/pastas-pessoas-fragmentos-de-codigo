#!/usr/bin/env python3

import time

time.sleep(0.25)

icon_name = clipboard.get_selection()

retCode, icon_name = dialog.input_dialog("Ícone original", "Para copiar os ícones simbólicos para outra pasta, " 
                                          + "\nescreva a ligação simbólica do ícone original")
if retCode == 0:
    keyboard.send_keys("cp -R $(find -L -samefile " + icon_name + ".svg) ~/Pictures/icons")
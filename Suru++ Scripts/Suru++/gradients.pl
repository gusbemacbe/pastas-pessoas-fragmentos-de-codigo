#!/bin/perl
 
my $replacement=<<EoF;
 <defs>
   <style id="current-color-scheme" type="text/css">
   .ColorScheme-Text { color: #ececec; } .ColorScheme-Highlight { color: #1AD6AB; }
  </style>
  <linearGradient id="arrongin" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#dd9b44" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#ad6c16" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="aurora" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#09D4DF" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#9269F4" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="cyberneon" x1="0%" x2="0%" y1="0%" y2="100%">
    <stop offset="0"    stop-color= "#0abdc6" stop-opacity="1"/>
    <stop offset="1"    stop-color= "#ea00d9" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="fitdance" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#1AD6AB" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#329DB6" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="oomox" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#efefe7" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#8f8f8b" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="rainblue" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#00F260" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#0575E6" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="sunrise" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#FF8501" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#FFCB01" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="telinkrin" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#b2ced6" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#6da5b7" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="60spsycho" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#df5940" stop-opacity="1"/>
   <stop offset="25%"   stop-color= "#d8d15f" stop-opacity="1"/>
   <stop offset="50%"   stop-color= "#e9882a" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#279362" stop-opacity="1"/>
  </linearGradient>
  <linearGradient id="90ssummer" x1="0%" x2="0%" y1="0%" y2="100%">
   <stop offset="0%"    stop-color= "#f618c7" stop-opacity="1"/>
   <stop offset="20%"   stop-color= "#94ffab" stop-opacity="1"/>
   <stop offset="50%"   stop-color= "#fbfd54" stop-opacity="1"/>
   <stop offset="100%"  stop-color= "#0f83ae" stop-opacity="1"/>
  </linearGradient>
 </defs>
EoF
## This is just to fix SE's syntax highlighting /    
my $foundSvg = 0;
while (<>) 
{
  ## Insert the replacement after the 1st line matching '<svg'
  if (/<\s*svg/) 
  {
    $foundSvg++;
  }
  if ($foundSvg == 1) 
  {
    ## $_ is the value of the current line. If we have found the <svg,
    ## append $replacement to this line
    $_ .= $replacement;
    ## Increment $foundSvg so we don't do this twice
    $foundSvg++;
  }
  ## For all lines, replace all occurrences of #ececec with ‘fill="currentColor" class="ColorScheme-Text"’
  s/style="fill:#ececec"/fill="currentColor" class="ColorScheme-Text"/g;
  s/style="fill:#ececec;opacity:0.3"/fill="currentColor" class="ColorScheme-Text" opacity="0.3"/g;
  s/style="opacity:0.3;fill:#ececec"/fill="currentColor" class="ColorScheme-Text" opacity="0.3"/g;
  s/style="fill:#5294e2" class="warning"/fill="currentColor" class="ColorScheme-Highlight warning"/g;
  s/style="opacity:0.35;fill:#5294e2" class="warning"/fill="currentColor" class="ColorScheme-Highlight warning" opacity="0.35"/g;
  s/style="fill:#5294ef;fill-opacity:1" class="warning"/fill="currentColor" class="ColorScheme-Highlight warning" opacity="1.0"/g;
  ## Print the line
  print;
}
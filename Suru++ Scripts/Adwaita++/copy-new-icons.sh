CAMINHO="$HOME/Pictures/icons" 

cd $HOME/GitHub/adwaita-plus-beta

cp Adwaita++/apps/16/{com.github.PintaProject.Pinta,evolution,figma,filled-xterm,mini.xterm,org.musicbrainz.Picard,pdfarranger,pdfmod,xterm-color,xterm-color_48x48,xterm}.svg $CAMINHO/apps/16
cp Adwaita++/apps/scalable/{com.github.PintaProject.Pinta,cups,evolution,figma,filled-xterm,firefox-official,mame,mini.xterm,org.gnome.Totem,org.musicbrainz.Picard,pdfarranger,pdfmod,thunderbird,vim,xnview,xterm,zenkit}.svg $CAMINHO/apps/scalable
cp Adwaita++/apps/symbolic/{com.github.PintaProject.Pinta,cups,evolution,figma,filled-xterm,firefox-official,mame,mini.xterm,org.musicbrainz.Picard,pdfarranger,pdfmod,thunderbird,vim,xnview,xterm,zenkit}-symbolic.svg $CAMINHO/apps/symbolic

cp Adwaita++/mimetypes/16/text-x-generic-template.svg $CAMINHO/mimetypes/16

cp Adwaita++/mimetypes/scalable/{application-x-7z-compressed,application-x-compressed-tar,application-x-gzip,application-x-iso9660-appimage,application-x-tar,application-x-webarchive,application-x-xz-compressed-tar,application-x-xz,package-gdebi,package,rar,zip}.svg $CAMINHO/mimetypes/scalable

cp Adwaita++/actions/16/{favorite-new,password}.svg $CAMINHO/actions/16

cp Adwaita++/status/scalable/{dialog-information,dialog-warning,gtk-dialog-info,messagebox_info,stock_dialog-info}.svg $CAMINHO/status/scalable
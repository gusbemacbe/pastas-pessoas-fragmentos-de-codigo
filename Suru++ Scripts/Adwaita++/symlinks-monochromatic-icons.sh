#!/bin/sh

ADWAITA="$HOME/GitHub/adwaita-plus-beta-gusbemacbe/Adwaita++"

echo "Adwaita++"
ln -sfnr $ADWAITA/apps/16/usb-creator.svg                 $ADWAITA/apps/16/woeusbgui-icon.svg
ln -sfnr $ADWAITA/apps/symbolic/usb-creator-symbolic.svg  $ADWAITA/apps/symbolic/woeusbgui-icon-symbolic.svg

echo "Adwaita++ Dark"
ln -sfnr $ADWAITA-Dark/apps/16/usb-creator.svg                $ADWAITA-Dark/apps/16/woeusbgui-icon-symbolic.svg
ln -sfnr $ADWAITA-Dark/apps/symbolic/usb-creator-symbolic.svg $ADWAITA-Dark/apps/symbolic/woeusbgui-icon-symbolic.svg
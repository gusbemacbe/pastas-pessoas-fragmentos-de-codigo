#!/bin/sh

ADWAITA="$HOME/GitHub/adwaita-plus-beta-gusbemacbe/Adwaita++"

echo "Adwaita++"
ln -sfnr $ADWAITA/apps/16/ulauncher-indicator.svg                 $ADWAITA/apps/16/ulauncher.svg
ln -sfnr $ADWAITA/apps/scalable/ulauncher-indicator.svg           $ADWAITA/apps/scalable/ulauncher.svg
ln -sfnr $ADWAITA/apps/symbolic/ulauncher-indicator-symbolic.svg  $ADWAITA/apps/symbolic/ulauncher-symbolic.svg

echo "Adwaita++ Dark"
ln -sfnr $ADWAITA-Dark/apps/16/ulauncher-indicator.svg                $ADWAITA-Dark/apps/16/ulauncher-symbolic.svg
ln -sfnr $ADWAITA-Dark/apps/symbolic/ulauncher-indicator-symbolic.svg $ADWAITA-Dark/apps/symbolic/ulauncher-symbolic.svg
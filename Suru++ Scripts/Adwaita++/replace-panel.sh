echo "Substituindo as cores..."
sed --in-place --follow-symlinks 's/#5c616c/#ececec/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:#ececec"/fill="#ececec"/g' *.svg

echo "Limpando os códigos sujos..."
sed --in-place --follow-symlinks 's/style="fill:currentColor"/fill="currentColor"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:currentColor;fill-rule:evenodd"/fill="currentColor" fill-rule="evenodd"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:none;stroke:currentColor;stroke-linejoin:round"/fill="none" stroke="currentColor" stroke-linejoin="round"/g' *.svg
sed --in-place --follow-symlinks 's/style="opacity:0.3;fill:currentColor;fill-rule:evenodd"/fill="currentColor" fill-rule="evenodd" opacity="0.3"/g' *.svg
sed --in-place --follow-symlinks 's/style="opacity:0.5;fill:currentColor"/fill="currentColor" opacity="0.5"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:currentColor;opacity:0.3" class="ColorScheme-Text"/fill="currentColor" class="ColorScheme-Text" opacity="0.3"/g' *.svg
sed --in-place --follow-symlinks 's/style="opacity:0.3;fill:currentColor" class="ColorScheme-Text"/fill="currentColor" class="ColorScheme-Text" opacity="0.3"/g' *.svg

echo "Purificando a classe SVG..."
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" fill="none" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http:\/\/www.w3.org\/2000\/svg">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" style="enable-background:new" width="16" height="16" version="1.1">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" style="isolation:isolate" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1" viewBox="0 0 16 16" style="isolation:isolate">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" xmlns:xlink="http:\/\/www.w3.org\/1999\/xlink" style="isolation:isolate" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg

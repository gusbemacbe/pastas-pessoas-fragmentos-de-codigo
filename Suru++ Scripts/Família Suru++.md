# Família Suru++

## Geral 

```bash
grep -rl 'Gravit' *.svg | sort
grep -rl 'style=' *.svg | sort
grep -rl 'fill:currentColor' *.svg | sort
grep -rL 'fill="currentColor"' *.svg | sort
grep -rL '.ColorScheme-Text { color:#dfdfdf; } .ColorScheme-Highlight { color:#4285f4; }' *.svg | sort
grep -o '.ColorScheme-Text { color:#dfdfdf; } .ColorScheme-Highlight { color:#4285f4; }' *.svg | wc -l

# Limpar os códigos sujos
sed --in-place --follow-symlinks 's/style="fill:currentColor"/fill="currentColor"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:currentColor;fill-rule:evenodd"/fill="currentColor" fill-rule="evenodd"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:none;stroke:currentColor;stroke-linejoin:round"/fill="none" stroke="currentColor" stroke-linejoin="round"/g' *.svg
sed --in-place --follow-symlinks 's/style="opacity:0.3;fill:currentColor;fill-rule:evenodd"/fill="currentColor" fill-rule="evenodd" opacity="0.3"/g' *.svg
sed --in-place --follow-symlinks 's/style="opacity:0.5;fill:currentColor"/fill="currentColor" opacity="0.5"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:currentColor;opacity:0.3" class="ColorScheme-Text"/fill="currentColor" class="ColorScheme-Text" opacity="0.3"/g' *.svg
sed --in-place --follow-symlinks 's/style="opacity:0.3;fill:currentColor" class="ColorScheme-Text"/fill="currentColor" class="ColorScheme-Text" opacity="0.3"/g' *.svg

# Perl
for f in *svg; do; perl ~/gradients.pl "$f" > tmpFile && mv tmpFile "$f"; done
for f in *svg; do; perl ~/gradients_actions.pl "$f" > tmpFile && mv tmpFile "$f"; done
for f in *svg; do; perl ~/gradients_emblems.pl "$f" > tmpFile && mv tmpFile "$f"; done
for f in *svg; do; perl ~/gradients_panel.pl "$f" > tmpFile && mv tmpFile "$f"; done
```

### 16 e symbolic

```bash
grep -rL '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" version="1.1">' *.svg | sort
grep -o '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" version="1.1">' *.svg | wc -l

sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" fill="none" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" style="enable-background:new" width="16" height="16" version="1.1">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" style="isolation:isolate" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1" viewBox="0 0 16 16" style="isolation:isolate">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" xmlns:xlink="http:\/\/www.w3.org\/1999\/xlink" style="isolation:isolate" width="16" height="16" viewBox="0 0 16 16">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="16" height="16" version="1.1">/g' *.svg 
```

### 22

```bash
grep -rL '<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" version="1.1">' *.svg | sort
grep -o '<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" version="1.1">' *.svg | wc -l
```

### 24

```bash
grep -rL '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" version="1.1">' *.svg | sort
grep -o '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" version="1.1">' *.svg | wc -l

# Limpar os códigos sujos
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="24" height="24" viewBox="0 0 24 24">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="24" height="24" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" style="isolation:isolate" width="24" height="24" viewBox="0 0 24 24">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="24" height="24" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="24" height="24" version="1.1" viewBox="0 0 24 24">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="24" height="24" version="1.1">/g' *.svg 
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" style="enable-background:new" width="24" height="24" version="1.1">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="24" height="24" version="1.1">/g' *.svg
```

### 32

```bash
grep -rL '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" version="1.1">' *.svg | sort
sed --in-place --follow-symlinks 's/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="32" height="32" version="1.1" viewBox="0 0 32 32">/<svg xmlns="http:\/\/www.w3.org\/2000\/svg" width="32" height="32" version="1.1">/g' *.svg 
```

### 48

```bash
grep -rL '<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" version="1.1">' *.svg | sort
```

## Suru++ Asprómauros e Suru++ Escuro

### Substituir as cores

```bash
# Substituir a cor do Suru++ pela do Suru++ Asprómauros e Escuro
sed --in-place --follow-symlinks 's/#5c616c/#ececec/g' *.svg
# Substituir o código de HTML pelo puro código de SVG
sed --in-place --follow-symlinks 's/style="fill:#ececec"/fill="#ececec"/g' *.svg
# Subsituir as cores do Papirus pelas cibercores do Suru++
sed --in-place --follow-symlinks 's/#5294e2/#4285f4/g' *.svg
sed --in-place --follow-symlinks 's/#dfdfdf/#ececec/g' *.svg
```

### apps

```bash
# Substituir o código de HTML pelo puro código de SVG
sed --in-place --follow-symlinks 's/style="fill:currentColor"/fill="currentColor"/g' *.svg
```

### emblems

```bash
sed --in-place --follow-symlinks 's/style="fill:#ffffff"/fill="#ececec"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:#252a35"/fill="url(#carbon)"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:#4caf50"/fill="url(#lemon)"/g' *.svg
sed --in-place --follow-symlinks 's/style="fill:#ffffff;fill-rule:evenodd"/fill="#ececec" fill-rule="evenodd"/g' *.svg
```

### panel

```bash
sed --in-place --follow-symlinks -z 's/ <defs>\n  <style id="current-color-scheme" type="text\/css">\n   .ColorScheme-Text { color:#ececec; } .ColorScheme-Highlight { color:#5294e2; }\n  <\/style>\n <\/defs>//g' *.svg
```
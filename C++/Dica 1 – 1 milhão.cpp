#include <algorithm>
#include <ctype.h>
#include <iostream>
#include <iomanip>
#include <locale>
#include <math.h>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

int main()
{
    setlocale(LC_ALL, "portuguese");
    float salarioSonho = 1E6,
          salarioReal = 30E-3;
    printf("Sonhei que meu salário era de R$%.2f, mas acordei e lembrei que era %.2f centavos.\n", salarioSonho, salarioReal);
    
  return 0;
}

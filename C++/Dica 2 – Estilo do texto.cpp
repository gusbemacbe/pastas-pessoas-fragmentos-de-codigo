#include <algorithm>
#include <ctype.h>
#include <iostream>
#include <iomanip>
#include <locale>
#include <math.h>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

int main()
{
    setlocale(LC_ALL, "portuguese");

    char esc_char = 27; // the decimal code for escape character is 27
    cout << esc_char << "[1m" << "Olá Negrito! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[2m" << "Olá Meio Transparente! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[3m" << "Olá Itálico! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[4m" << "Olá Sublinhado! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[5m" << "Olá Piscado! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[7m" << "Olá Fundo Cheio! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[8m" << "Olá Escondido! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[20m" << "Olá Fraktur! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[30m" << "Olá Preto! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[31m" << "Olá Vermelho! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "[32m" << "Olá Verde! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "\033[38;5;161m" << "Olá Rosa Gostosa! Sou Gustavo." << esc_char << "[0m" << endl;
    cout << esc_char << "\033[38;5;64m" << "Olá Verde Moderado! Sou Gustavo." << esc_char << "[0m" << endl;

  return 0;
}

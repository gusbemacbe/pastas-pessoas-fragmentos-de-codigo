#include <iostream>
#include <string>
#ifdef _MSC_VER
#include "windows.h"
#endif
int main()
{
	const char *HORZ = "─"; // HORiZontal line
	const char *VERT = "│"; // VERTical line
	const char *TL = "┌";		// Top Left
	const char *TR = "┐";		// Top Right
	const char *BL = "└";		// Bottom Left
	const char *BR = "┘";		// Bottom Right
	char SPACE = 12;				// Space

	std::string line1 = TL;
	std::string line3 = BL;

	for (int n = 0; n < 30; ++n)
	{
		line1 += HORZ;
		line3 += HORZ;
	}

	line1 += TR;
	line3 += BR;
	std::string line2 = std::string(30, SPACE) + std::string(" Hello, world! ");

#ifdef _MSC_VER
	SetConsoleOutputCP(CP_UTF8);
#endif

	std::cout << line1 << '\n'
						<< line2 << '\n'
						<< line3 << '\n';
}

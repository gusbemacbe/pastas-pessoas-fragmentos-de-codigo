<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Star Wars Year Converter</title>
    <style media="screen">
      body {
        font-family: "Segoe UI", sans-serif;
      }
    </style>
  </head>
  <body>
    <h1>Descubra em qual ano de Batalha de Yavin do filme Star Wars você nasceu!</h1>
    
    <p><b>ABY =</b> After Battle of Yavin = Depois da Batalha de Yavin.</p>
    <p><b>BBY =</b> Before Battle of Yavin = Antes da Batalha de Yavin.</p>
    <form action="dates.php" method="post">
     <p>Seu ano de nascimento: <input type="text" name="ano" /></p>
     <p><input type="submit" /></p>
    </form>
  </body>
</html>

<?php 

// ABY = depois da Batalha de Yavin
// BBY = antes da Batalha de Yavin

if (isset($_POST['ano']))
{
  $ano = (int)$_POST["ano"];

  if ($ano < 1977)
  {
    $resultado = 1977 - $ano;
    echo "Você nasceu no ano $resultado BBY <br>";
    
    $ruusan = 1000 - $resultado;
    echo "Você nasceu no ano $ruusan de Reforma de Ruusan <br>";
    
    $ly = 3277 - $resultado;
    echo "Você nasceu no " . $ly . "° Ano de Lothal <br>";
    
    $gsc = 25043 - $resultado;
    echo "Você nasceu no ano " . $gsc . " do Calendário Padrão Galáctico";
  } 
  
  elseif ($ano == 1977)
  {
    $resultado = 1977 - $ano;
    echo "Você nasceu no ano $resultado BBY <br>";
    
    $ruusan = 1000 + $resultado;
    echo "Você nasceu no ano $ruusan de Reforma de Ruusan <br>";
    
    $ly = 3277 + $resultado;
    echo "Você nasceu no " . $ly ."° Ano de Lothal <br>";
    
    $gsc = 25043 + $resultado;
    echo "Você nasceu no ano " . $gsc . " do Calendário Padrão Galáctico";
  }
  
  else {
    $resultado = $ano - 1977;
    echo "Você nasceu no ano $resultado ABY <br>";
    
    $ruusan = 1000 + $resultado;
    echo "Você nasceu no ano $ruusan de Reforma de Ruusan <br>";
    
    $ly = 3277 + $resultado;
    echo "Você nasceu no " . $ly . "° Ano de Lothal <br>";
    
    $gsc = 25043 + $resultado;
    echo "Você nasceu no ano " . $gsc . " do Calendário Padrão Galáctico";
  }
}

echo "<h1>Datas secularistas e isentas de religiões</h1>";

if (isset($_POST['ano']))
{
  $ano = (int)$_POST["ano"];
  
  echo "<h2>Ano de Revolução Francesa</h2>";
  
  if ($ano < 1888)
  {
    $resultado = 1888 - $ano;
    echo "Você nasceu no ano $resultado antes da Revolução Francesa <br>";
  } 
  
  elseif ($ano == 1888)
  {
    $resultado = 1888 - $ano;
    echo "Você nasceu no ano $resultado da Revolução Francesa <br>";
  }
  
  else {
    $resultado = $ano - 1888;
    echo "Você nasceu no ano $resultado depois da Revolução Francesa <br>";
  }
  
  echo "<h2>Ano da Revolução do Computador</h2>";
  
  if ($ano < 1970)
  {
    $computador = 1970 - $ano;
    echo "Você nasceu no ano $computador antes da Revolução do Computador <br>";
  } 
  
  elseif ($ano == 1970)
  {
    $computador = 1970 + $ano;
    echo "Você nasceu no ano $computador do nascimento da Revolução de Nascimento <br>";
  }
  
  else {
    $computador = $ano - 1970;
    echo "Você nasceu no ano $computador depois da Revolução do Computador <br>";
  }
  
}

?>
<?php

// Números reduzidos

function numero_reduzido($n) 
{    
  $n = (0+str_replace(",", "", $n));
    
  if(!is_numeric($n)) return false;
  
  if ($n > 1000000000000) 
    return round(($n / 1000000000000), 1) .' trillion';
  elseif ($n > 1000000000) 
    return round(($n / 1000000000), 1) .' billion';
  elseif ($n > 1000000) 
    return round(($n / 1000000), 1) .' million';
  elseif ($n > 1000) 
    return round(($n / 1000), 1) .' thousand';
  
  return number_format($n);
}

?>
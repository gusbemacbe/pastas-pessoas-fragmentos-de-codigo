<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Teste do artigo antes da palavra em alemão</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  
  <body>
      <h1>Teste do artigo antes da palavra em alemão</h1>

        <form action="index.php" method="post">
          <label for="word">Palavra</label>
          <input type="text" id="word" name="word" placeholder="Palavra">

          <input type="submit" value="Ver">
        </form>
        
        <?php 
        if (!empty($_POST["word"])) 
        {
          $word = strtolower($_POST["word"]); 

          $dictionary = 
          [
            "das" => ["kind", "licht", "mädchen"],
            "der" => ["hund", "kater", "mann", "storm", "winter"],
            "die" => ["dame", "frau", "katze"]
          ];

          $found = false;

          foreach ($dictionary as $group => $words)
          {
            if (in_array($words, $word)) 
            {
              echo "É <b>" . $group . "</b>.";
              $found = true;
              break;
            }
          }

          if (!$found)
          {
            echo "Não foi encontrado.";
          }
        }
        ?>
  </body>
</html>
function sortComparer(a, b)
{
    return a.title.localeCompare(b.title)
};

const reA = /[^a-z\u00E0-\u00FC]/g;
const reN = /[^0-9]/g;

function sortAlphaNum(a, b) 
{
  const aA = a.replace(reA, "");
  const bA = b.replace(reA, "");
  if (aA === bA) 
  {
    const aN = parseInt(a.replace(reN, ""), 10);
    const bN = parseInt(b.replace(reN, ""), 10);
    return aN === bN ? 0 : aN > bN ? 1 : -1;
  } 
  else 
  {
    return aA > bA ? 1 : -1;
  }
}

console.log(
  ["3", "2", "10", "40", "6", "4", "30", "33", "1", "Gustavo", "julho", "Klaus", "keyboard", "último", "árbol", "uma", "água", "Argentina", "Ángelo", "argelino", "unido"].sort(sortComparer(sortAlphaNum))
)

// Intl.Collator().compare
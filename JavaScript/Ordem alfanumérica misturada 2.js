
const alphabets = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "A", "á", "Á", "b", "B", "c", "C", "č", "Č", "ć", "Ć", "d", "D", "dž","Dž", "đ", "Đ", "e", "E","F","G","H","I", "j", "J","K","L","Lj","M","N","Nj","O","P","R","S","ÛŒ", "T", "u", "U", "ú", "Ú", "V","Z","Ž"];
const testArrray = ["3", "2", "10", "40", "6", "4", "30", "33", "1", "Gustavo", "julho", "Klaus", "Ιαπωνία", "keyboard", "სკოლა", "último", "árbol", "γυναίκα", "uma", "água", "Argentina", "Ángelo", "argelino", "unido", "женщина", "κήπος", "друг", "дом", "ბაღი", "люди"];

function OrderFunc()
{
   testArrray.sort(function (a, b) 
   {
       return CharCompare(a, b, 0);
   });
   
   document.getElementById("result").innerHTML = testArrray;
}

function CharCompare(a, b, index) 
{
  if (index == a.length || index == b.length)
      return 0;
  const aChar = alphabets.indexOf(a.toUpperCase().charAt(index));
  const bChar = alphabets.indexOf(b.toUpperCase().charAt(index));

  if (aChar != bChar)
      return aChar - bChar
  else
      return CharCompare(a,b,index+1)
}
function naturalCompare(a, b) 
{
    const ax = [], bx = [];

    a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
    
    while(ax.length && bx.length) 
    {
        const an = ax.shift();
        const bn = bx.shift();
        const nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
        if(nn) return nn;
    }

    return ax.length - bx.length;
}

test01 = ["3", "2", "10", "40", "6", "4", "30", "33", "1", "Gustavo", "julho", "Klaus", "Ιαπωνία", "keyboard", "სკოლა", "último", "árbol", "γυναίκα", "uma", "água", "Argentina", "Ángelo", "argelino", "unido", "женщина", "κήπος", "друг", "дом", "ბაღი", "люди"];
test02 = ["a", "A", "à", "À", "â", "Â", "c", "C", "ç", "Ç", "e", "E", "é", "É", "è", "È", "ê", "Ê", "ë", "Ë"];
test03 = ["surélévation", "sûrement", "suréminent", "sûreté"];

console.log(test03.sort(naturalCompare))
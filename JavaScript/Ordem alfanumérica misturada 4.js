collator = new Intl.Collator('es', 
{
    caseFirst: "lower",
    ignorePunctuation: true,
    localeMatcher: "best fit",
    numeric: true,
    sensitivity: 'variant',
    usage: 'sort'
});

test01 = ["3", "2", "10", "40", "6", "4", "30", "33", "1", "Gustavo", "julho", "Klaus", "Ιαπωνία", "keyboard", "სკოლა", "último", "árbol", "γυναίκα", "uma", "água", "äntlich", "Änatur", "Österreich", "argentina", "Ángelo", "argelino", "unido", "женщина", "κήπος", "друг", "дом", "ბაღი", "люди"];
test02 = ["a", "A", "à", "À", "â", "Â", "c", "C", "ç", "Ç", "e", "E", "é", "É", "è", "È", "ê", "Ê", "ë", "Ë"];

// Correct order:  surélévation sûrement suréminent sûreté 

test03 = ["suréminent", "surélévation", "sûreté", "sûrement"];

console.log(test03.sort(collator.compare));